<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>
<body>
    <?php
        function tentukan_nilai($number){
            if  ($number >= 98 ) {
                $output = ' Sangat Baik';
            } else if ( $number >= 76 & $number <= 98 ) {
                $output = ' Baik';
            } else if ( $number >= 67 & $number <= 76 ) {
                $output = ' cukup';
            } else {
                $output = ' Kurang';
            }
           echo "<br>";
           echo "$number";
           echo "$output";
           
           
       }

        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang
    ?>
</body>
</html>